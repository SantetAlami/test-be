<?php

function panjang($input)
{
    $text = explode(" ", $input);

    $res = "";
    $putar = $text[0];
    foreach ($text as $key => $value) {
        if (strpos($value,strrev($putar)) !== false){
            $res = $putar . " " . $value;
        }
        $putar = $value;
    }

    return $res;
}

echo panjang("aku suka makan nasi"). "\n" .
    panjang("di rumah saya ada kasur rusak"). "\n" .
    panjang("abcde edcbza");