SELECT 
	t.id,
	t.tanggal_order, 
	t.status_pelunasan, 
	t.tanggal_pembayaran,
	SUM(td.harga) total,
	COUNT(td.id) jumlah_barang,
	""
FROM transaksi t
LEFT JOIN transaksi_detail td ON td.id_transaksi = t.id
GROUP BY t.id