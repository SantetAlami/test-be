CREATE TABLE transaksi (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    tanggal_order TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    status_pelunasan VARCHAR(30) NOT NULL,
    tanggal_pembayaran TIMESTAMP DEFAULT NULL
);
insert into transaksi (tanggal_order, status_pelunasan, tanggal_pembayaran) 
VALUES
    (1, '2020-12-01 11:31:00', 'lunas', '2020-12-01 12:00:00'),
    (2, '2020-12-01 12:20:00', 'pending', NULL),
    (3, '2020-12-01 14:42:00', 'lunas', '2020-12-01 15:00:00'),
    (4, '2020-12-01 11:10:00', 'lunas', '2020-12-01 12:00:00'),
    (5, '2020-12-02 10:30:00', 'pending', NULL)