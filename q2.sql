CREATE TABLE transaksi_detail (
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    id_transaksi INT(11) NOT NULL,
    harga int(50) NOT NULL,
    jumlah int(50) NOT NULL,
    subtotal int(50) NOT NULL
);

insert into
    transaksi_detail (id_transaksi, harga, jumlah, subtotal)
VALUES
    (1, 20000, 2, 40000),
    (1, 40000, 2, 80000),
    (1, 50000, 2, 100000),
    (1, 10000, 2, 20000),
    (2, 25000, 4, 100000),
    (2, 40000, 2, 80000),
    (2, 50000, 2, 100000),
    (3, 10000, 2, 20000),
    (4, 40000, 2, 80000),
    (4, 50000, 2, 100000),
    (5, 20000, 2, 40000);